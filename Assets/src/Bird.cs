using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {
    public Rigidbody2D rb;

    public float velocidadImpulso;

    public int puntaje;

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        if (Input.GetMouseButtonDown(0) || Input.touches.Length > 0) {
            rb.velocity = Vector2.up * velocidadImpulso;
        }
    }
}